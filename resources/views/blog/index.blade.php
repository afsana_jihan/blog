@extends('master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <br />
            <h3 align="center">All Post</h3>
        </div>
            <br />
            @if($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{$message}}</p>
                </div>
            @endif
    </div>
    <div class="row">
        <div class="col-md-2">

        </div>
        <form class="form-inline" action="/search" method="get">
            @csrf
            <div class="col-md-6 text-right">
                <input type="search" name="search" class="form-control" placeholder="search post here">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </form>
            <div class="col-md-4 text-right">
                <a href="{{route('post.create')}}" class="btn btn-primary">Add</a>
                <br />
                <br />
            </div>
    </div>

            <table class="table table-bordered table-striped">
                <tr>
                    <th>NAME</th>
                    <th>EDIT</th>
                    <th>DELETE</th>
                </tr>

                @foreach($posts as $row)
                    <tr>
                        <td>{{$row['name']}}</td>
                        <td><a href="{{action('PostController@edit',$row['id'])}}" class="btn btn-info">Edit</a></td>
                        <td>
                        <form method="POST" action="{{route('post.destroy',$row['id'])}}">
                            @csrf
                            @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                        </td>
                    </tr>
                @endforeach
            </table>

@endsection
