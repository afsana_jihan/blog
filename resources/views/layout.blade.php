<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title','Laracasts')</title>

</head>
<body>
    @yield('content')
    <a href="/">HOME</a></p>
    <a href="/contact">Contact</a></p>
    <a href="/about">about us</a></p>
</body>
</html>
